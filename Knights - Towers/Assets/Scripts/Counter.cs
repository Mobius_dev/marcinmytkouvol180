﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour {

    public int towers = 0;
    public Text counterText;
    public bool towersSpawning = true;

    private void Update()
    {
        counterText.text = "Towers: " + towers;

        if (towers >= 100) towersSpawning = false;
    }

    public void AddTower()
    {
        towers++;
    }

    public void RemoveTower()
    {
        towers--;
    }

    

}
