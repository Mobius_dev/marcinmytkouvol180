﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    private Rigidbody2D rb;
    public Transform tower;
    [HideInInspector]
    public Vector3 direction;
    [HideInInspector]
    public bool canSpawn = true;

    void Awake()
    {
        float _time = Random.Range(0.25f, 1 + Mathf.Epsilon);
        Invoke("DestroyAndSpawn", _time);
    }

    void DestroyAndSpawn()
    {
        if (canSpawn) Instantiate(tower, this.transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.gameObject.tag == "tower")
        {
            Destroy(collision.transform.gameObject);
            Destroy(this.gameObject);
        }
    }
}
