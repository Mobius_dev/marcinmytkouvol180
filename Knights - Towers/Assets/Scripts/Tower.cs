﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour {
    public Transform projectile;
    public Transform spawnPoint;
    public bool isFirstTower; //set this to true if this the original tower
    private int shotsFired;
    private SpriteRenderer sr;
    private Counter counter;
    private bool projectilesCanSpawn = true;

    void Awake()
    {
        counter = GameObject.Find("Tower Counter").transform.GetComponent<Counter>();
        counter.AddTower();

        sr = GetComponent<SpriteRenderer>();

        if (!isFirstTower)
        {
            InvokeRepeating("RotateAndFire", 6f, 0.5f);
        }
        else
        {
            InvokeRepeating("RotateAndFire", 0, 0.5f);
        }

    }

    private void Update()
    {

        if (!counter.towersSpawning && projectilesCanSpawn)
        {
            projectilesCanSpawn = false;
            CancelInvoke();
            InvokeRepeating("RotateAndFire", 0, 0.5f);
        }
    }

    void RotateAndFire()
    {
        sr.color = new Color(255, 0, 0, 255);

        float _degreesToRotate = Random.Range(15, 46);
        this.transform.Rotate(new Vector3(0, 0, _degreesToRotate));

        Transform _new = Instantiate(projectile, spawnPoint.position, Quaternion.identity);
        Projectile _projectile = _new.GetComponent<Projectile>();
        Rigidbody2D _rb = _new.GetComponent<Rigidbody2D>();
        Vector3 _direction = (_new.position - this.transform.position).normalized;
        _projectile.direction = _direction;
        _rb.velocity = _direction * 4;
        if (!projectilesCanSpawn) _projectile.canSpawn = false; 


        shotsFired++;

        if (shotsFired == 12 && projectilesCanSpawn)
        {
            sr.color = new Color(255, 255, 255, 255);
            CancelInvoke();
        }
    }

    private void OnDestroy()
    {
        counter.RemoveTower();
    }
}
